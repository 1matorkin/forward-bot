import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

@SingletonEntity
@Access({ 'GET': 0, 'PUT': 0 }, { single: true, icon: 'gear' })
@Entity()
export class BotSettings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /^\d{9}:[\w-]{35}$/, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    telegramBotToken: string;

    @v({ notEqual: 0, description: 'Узнать id аккаунта телеграм можно с помощью бота [@myidbot](tg://resolve?domain=myidbot)' })
    @Column({ default: -1 })
    operatorId: number;
}