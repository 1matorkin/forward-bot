import { BotSettings } from '@entities/BotSettings';
import { Dialog } from '@entities/Dialog';
import { Settings } from '@entities/Settings';
import { User } from '@entities/User';

export default [
    Settings,
    BotSettings,
    User,
    Dialog,
]