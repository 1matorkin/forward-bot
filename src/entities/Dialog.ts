import { Column, Entity, PrimaryColumn } from 'typeorm';

import { Access } from '@crud';

@Access({ 'GET': 0, 'DELETE': 0 }, { icon: 'list' })
@Entity()
export class Dialog {
    @PrimaryColumn({ default: 0 })
    chat: number;

    @Column({ default: '' })
    user: string;
}