import { Context } from 'koa';
import * as TelegramBot from 'node-telegram-bot-api';
import * as getRawBody from 'raw-body';
import { Repository } from 'typeorm';
import { Inject, Singleton } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';
import { Dialog } from '@entities/Dialog';
import { Settings } from '@entities/Settings';
import { BaseRequestOpts, promiseRequest } from '@utils/cancelable-request';
import rndString, { rndStringSync } from '@utils/rnd-string';
import { post } from '@utils/routes-helpers';
import stringify from '@utils/stringify-error';
import trim from '@utils/trim';

import { DBConnection } from './DBConnection';

function trunc(s: string) {
    const truncBuffer = Buffer.alloc(30, 0);
    const bytesWritten = truncBuffer.write(s, 0, 30, 'utf-8');
    return truncBuffer.toString('utf8', 0, bytesWritten);
}

function name(user?: TelegramBot.User) {
    return user ? trunc(trim(user.username || (user.last_name || '') + ' ' + (user.first_name || ''))) : '<anonym>';
}

function button(text: string, i: number) {
    return { text, callback_data: JSON.stringify({ i, t: text }) };
}

@Singleton
export class Bot {
    @Inject private readonly settings: Settings;
    @Inject private readonly botSettings: BotSettings;

    private bot: TelegramBot = null;
    private lastWebHookToken: string = rndStringSync();
    private readonly dialogsRepository: Repository<Dialog>;
    private readonly settingsRepository: Repository<Settings>;
    private latestWaitMessage: TelegramBot.Message = null;
    private operatorId = 0;

    constructor(@Inject connection: DBConnection) {
        this.dialogsRepository = connection.getRepository(Dialog);
        this.settingsRepository = connection.getRepository(Settings);
    }

    static async validateToken(token: string): Promise<boolean> {
        if (token) {
            try {
                const { body } = await promiseRequest('GET', `https://api.telegram.org/bot${token}/getMe`);
                return JSON.parse(body).result.is_bot;
            } catch (_) { }
        }
        return false;
    }

    async showDialogsList(dialogs: Dialog[], chat: number) {
        let inline_keyboard: any[][] = [];
        for (let dialog of dialogs) {
            if (dialog.chat != this.settings.operatorChat) {
                const btn = button(dialog.user, dialog.chat);
                const a = inline_keyboard[inline_keyboard.length - 1];
                if (a && a.length < 3) {
                    a.push(btn);
                } else {
                    inline_keyboard.push([btn]);
                }
            }
        }
        if (inline_keyboard.length > 0) {
            await this.sendKeyboard(chat);
            await this.bot.sendMessage(chat, 'Диалоги:', {
                disable_notification: true,
                reply_markup: {
                    inline_keyboard
                },
                parse_mode: 'Markdown'
            });
        } else {
            await this.sendKeyboard(chat, 'Нет активных диалогов.');
        }
    }

    async tryFindDialog() {
        try {
            const chat = this.activeChat;
            if (chat) {
                return 'Активный диалог: ' + (await this.dialogsRepository.findOneOrFail({ chat })).user;
            }
        } catch (_) { }
        return '`ׅ`';
    }

    async sendKeyboard(chat: number, text?: string) {
        return await this.bot.sendMessage(chat, text || await this.tryFindDialog(), {
            disable_notification: true,
            reply_markup: {
                resize_keyboard: true,
                keyboard: [[{ text: '/show_dialogs' }]]
            },
            parse_mode: 'Markdown'
        });
    }

    set activeChat(chat: number) {
        if (chat != this.settings.activeChat) {
            this.latestWaitMessage = null;
            this.settings.activeChat = chat;
        }
    }

    get activeChat() {
        return this.settings.activeChat;
    }

    async changeActiveChat(chat: number, user: string, callback_query_id?: string) {
        const needUpdateSettings = chat && chat != this.activeChat;
        try {
            if (needUpdateSettings) {
                this.activeChat = chat;
            }
            if (callback_query_id) {
                await this.bot.answerCallbackQuery({
                    callback_query_id,
                    show_alert: true,
                    text: 'Активный диалог: ' + user
                });
            } else if (this.settings.operatorChat) {
                await this.bot.sendMessage(this.settings.operatorChat, 'Активный диалог: ' + user);
            }
        } finally {
            if (needUpdateSettings) {
                await this.settingsRepository.save(this.settings);
            }
        }
    }

    async sendError(err) {
        console.error(err);
        try {
            if (this.settings.operatorChat) {
                await this.bot.sendMessage(this.settings.operatorChat, stringify(err));
            }
        } catch (_) { }
    }

    changeOperatorChat(chat: number) {
        this.settings.operatorChat = chat;
        if (this.activeChat == chat) {
            this.activeChat = 0;
        }
        return this.settingsRepository.save(this.settings);
    }

    async isAdminMessage(message: TelegramBot.Message) {
        const isAdmin = message.from && message.from.id == this.botSettings.operatorId;
        const chat = message.chat.id;
        if (isAdmin && this.settings.operatorChat != chat) {
            await this.changeOperatorChat(chat);
        } else if (this.operatorId != this.botSettings.operatorId) {
            await this.changeOperatorChat(isAdmin ? chat : 0);
        }
        this.operatorId = this.botSettings.operatorId;
        return isAdmin;
    }

    async update(token: string) {
        if (token && token == this.token) {
            return true;
        }
        if (await Bot.validateToken(token)) {
            try {
                if (token != this.token) {
                    const { body } = await promiseRequest('GET', {
                        url: `https://api.telegram.org/bot${token}/setWebhook`,
                        form: {
                            url: this.settings.telegramHookUrl + '/' + await this.rotateWebHookToken(),
                            allowed_updates: [
                                'message', 'edited_message',
                                'callback_query'//for admin
                            ]
                        }
                    });

                    if (JSON.parse(body).result != true) {
                        throw body.toString();
                    }

                    this.bot = new TelegramBot(token, { request: <any>BaseRequestOpts });

                    this.bot.on('message', async (message: TelegramBot.Message) => {
                        try {
                            const text = trim(message.text);
                            const isAdmin = await this.isAdminMessage(message);
                            const chat = message.chat.id;

                            if (isAdmin && (text == '/show_dialogs' || text == '/start')) {
                                await this.showDialogsList(await this.dialogsRepository.find(), chat);
                            } else {
                                if (isAdmin) {
                                    if (!this.activeChat) {
                                        throw 'Не выбран активный диалог.';
                                    }
                                    if (text) {
                                        try {
                                            await this.bot.editMessageText(text, {
                                                chat_id: this.latestWaitMessage.chat.id,
                                                message_id: this.latestWaitMessage.message_id
                                            });
                                        } catch (_) {
                                            await this.bot.sendMessage(this.activeChat, text);
                                        } finally {
                                            this.latestWaitMessage = null;
                                        }
                                    } else {
                                        throw 'Поддерживается перессылка только текстовых сообщений.';
                                    }
                                } else {
                                    const user = name(message.from);
                                    try {
                                        const d = new Dialog();
                                        d.chat = chat;
                                        d.user = user;
                                        await this.dialogsRepository.insert(d, { reload: false });
                                    } catch (_) { }
                                    if (!this.activeChat) {
                                        await this.changeActiveChat(chat, user);
                                    }
                                    if (text != '/start') {
                                        const { operatorChat } = this.settings;
                                        const [latestWaitMessage] = await Promise.all([
                                            this.bot.sendMessage(chat, 'Сообщение обрабатывается...', { disable_notification: true }),
                                            (operatorChat ? this.bot.forwardMessage(operatorChat, chat, message.message_id) : null),
                                        ]);
                                        if (!(latestWaitMessage instanceof Error)) {
                                            this.latestWaitMessage = latestWaitMessage;
                                        }
                                    }
                                }
                            }
                        } catch (err) {
                            await this.sendError(err);
                        }
                    });

                    this.bot.on('edited_message', async (message: TelegramBot.Message) => {
                        try {
                            const isAdmin = await this.isAdminMessage(message);
                            if (isAdmin) {
                                throw 'Редактирование сообщений пока находится в разработке.';
                            } else {
                                const { operatorChat } = this.settings;
                                if (operatorChat) {
                                    await this.bot.forwardMessage(operatorChat, message.chat.id, message.message_id);
                                }
                            }
                        } catch (err) {
                            await this.sendError(err);
                        }
                    });

                    this.bot.on('callback_query', async (c: TelegramBot.CallbackQuery) => {
                        try {
                            const { i, t } = c.data && JSON.parse(c.data);
                            await this.changeActiveChat(i, t, c.id);
                        } catch (err) {
                            await this.sendError(err);
                        }
                    });
                }
                return true;
            } catch (err) {
                console.error(err);
            }
        }
        return false;
    }

    @post({ path: '/process-telegram-messages/:token' })
    async processTelegramMessages(ctx: Context) {
        if (ctx.params.token === this.lastWebHookToken && this.bot) {
            const updates = JSON.parse(await getRawBody(ctx.req, { limit: 1024 * 1024 * 10, encoding: 'utf-8' }));
            this.bot.processUpdate(updates);
        }
        ctx.status = 200;
        ctx.body = '';
    }

    async rotateWebHookToken() {
        return this.lastWebHookToken = await rndString();
    }

    get token() {
        //@ts-ignore
        return (this.bot && this.bot.token) || '';
    }
}