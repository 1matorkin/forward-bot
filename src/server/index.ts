import * as Koa from 'koa';
import * as mount from 'koa-mount';
import * as send from 'koa-send';
import * as session from 'koa-session';
import { Container, Inject } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';
import { Settings } from '@entities/Settings';

import { Bot } from './Bot';
import { dbInit } from './DBConnection';
import { MainRouter } from './MainRouter';
import { ADMIN, STATIC_DIR } from './paths.config';

if (!(DEBUG || typeof v8debug === 'object' || /--debug|--inspect/.test(process.execArgv.join(' ')))) {
    require('pretty-error').start();
}

class App {
    private readonly app: Koa;

    get koaApp() {
        return this.app;
    }

    constructor(
        @Inject settings: Settings,
        @Inject router: MainRouter,
        @Inject bot: Bot,
        @Inject parserSettings: BotSettings
    ) {
        bot.update(parserSettings.telegramBotToken);

        const app = this.app = new Koa();

        //load secret from persistent storage (required by koa-session)
        app.keys = [settings.secret];
        app
            // sessions
            .use(session({
                key: '_',
                maxAge: 86400000 * 365,
                renew: true,
                httpOnly: true,
                signed: true
            }, app))
            //routes
            .use(router.routes())
            .use(router.allowedMethods())
            //static assets
            .use(mount(ADMIN, ctx => send(ctx, ctx.path, {
                root: STATIC_DIR,
                maxage: 365 * 24 * 60 * 60 * 1000,
                gzip: false,
                brotli: !DEBUG
            })))
    }
}
async function main() {
    try {
        await dbInit();

        const { koaApp } = Container.get(App);

        const port = (+process.env.PORT >> 0) || 8080;
        const ip = process.env.IP || '0.0.0.0';

        await new Promise(resolve => koaApp.listen(port, ip, resolve));

        console.log(`Server listening on http://${ip}:${port}/`);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

main();