import { join } from 'path';

export const ADMIN = '/admin';
export const API = '/api';
export const HOOKS = '/hooks';
export const STATIC_DIR = join(__dirname + '/static/');