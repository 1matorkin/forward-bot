import { Context } from 'koa';

import err2str from '@utils/stringify-error';

export default (redirectRouteName: string) => async function (ctx: Context, next: () => Promise<any>) {
    try {
        await next();
    } catch (err) {
        if (Array.isArray(err)) {
            for (let e of err) {
                ctx.addFlash(err2str(e));
            }
        } else {
            ctx.addFlash(err2str(err));
        }
        ctx.namedRedirect(redirectRouteName);
    }
}