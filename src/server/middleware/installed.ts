import { Context } from 'koa';

import { Settings } from '@entities/Settings';

export default (settings: Settings) => function (ctx: Context, next: () => Promise<any>) {
    if (settings.installed) {
        return next();
    } else {
        ctx.namedRedirect('install');
    }
}