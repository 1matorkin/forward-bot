import { Context } from 'koa';

type FlashData = Readonly<{
    messages: string[],
    warnings: string[],
    errors: string[]
}>

export const enum FlashLevel {
    ERROR = 0,
    WARNING = 1,
    INFO = 2,
}

declare module 'koa' {
    interface Context {
        flash?: FlashData;
        addFlash: (message: string, level?: FlashLevel) => void;
    }
}

const cookieOpts = Object.freeze({
    maxAge: 300 * 1000,
    path: null,
    domain: null,
    secure: false,
    httpOnly: false,
    sameSite: false,
    signed: false,
    overwrite: true
});

const deleteCookieOpts = Object.freeze({ ...cookieOpts, maxAge: -1 });

export default async (ctx: Context, next: () => Promise<any>) => {
    let prev = ctx.cookies.get('_f');
    if (prev) {
        try {
            const t = JSON.parse(decodeURIComponent(prev));
            ctx.flash = {
                messages: t[FlashLevel.INFO],
                warnings: t[FlashLevel.WARNING],
                errors: t[FlashLevel.ERROR]
            };
        } catch (_) { }
    }

    let a: string[][] = [[], [], []];

    ctx.addFlash = (message: string, level?: FlashLevel) => {
        a[level >> 0].push(message);
    }

    await next();

    if (a.some(v => v.length > 0)) {
        ctx.cookies.set('_f', encodeURIComponent(JSON.stringify(a)), cookieOpts);
    } else if (prev) {
        ctx.cookies.set('_f', '', deleteCookieOpts);
    }
}