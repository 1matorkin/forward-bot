import { Context } from 'koa';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.isAuthenticated()) {
        return next();
    } else {
        ctx.namedRedirect('login');
    }
}