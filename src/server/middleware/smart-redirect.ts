import { Context } from 'koa';
import * as Router from 'koa-router';

import resolveUrl from '@utils/resolve-url';

declare module 'koa' {
    interface Context {
        router: Router;
        namedRedirect: (routName: string) => void;
        resolve: (routName: string) => string;
    }
}

const routsCache = Object.create(null);
const routsCacheHttps = Object.create(null);

export default (ctx: Context, next: () => Promise<any>) => {

    const rcache = ctx.secure || ctx.request.header['x-forwarded-proto'] === 'https' ? routsCacheHttps : routsCache;

    ctx.resolve = routeName => rcache[routeName] || (rcache[routeName] = resolveUrl(ctx, ctx.router.url(routeName, null)));

    ctx.namedRedirect = routName => ctx.redirect(ctx.resolve(routName));

    return next();
}