import { ParserSettingsSubscriber } from '@subscribers/ParserSettingsSubscriber';
import { ValidateSubscriber } from '@subscribers/ValidateSubscriber';

export default [
    ValidateSubscriber,
    ParserSettingsSubscriber
];