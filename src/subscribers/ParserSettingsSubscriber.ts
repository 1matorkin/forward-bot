import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { BotSettings } from '@entities/BotSettings';

import { Bot } from '../server/Bot';

@EventSubscriber()
export class ParserSettingsSubscriber implements EntitySubscriberInterface<BotSettings> {

    listenTo() {
        return BotSettings;
    }

    async beforeUpdate({ entity }: UpdateEvent<BotSettings>) {
        if (!await Container.get(Bot).update(entity.telegramBotToken)) {
            throw [{
                field: 'telegramBotToken',
                message: 'Invalid telegram token'
            }];
        }
    }
}