import { promiseRequest } from '@utils/cancelable-request';

export default async (form: { email: string, [x: string]: any }) => {
    try {
        const { body } = await promiseRequest('POST', {
            url: 'https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec',
            form
        });
        if (JSON.parse(body).success != 'ok') {
            throw null;
        }
    } catch (err) {
        err && console.error(err);
        throw 'Email sending error. Try again later.';
    }
}