//@ts-ignore
const decoder = window.TextDecoder && new TextDecoder('utf-8');

export default (ab: ArrayBuffer) => new Promise((resolve, reject) => {
    if (decoder) {
        const s = decoder.decode(ab);
        try {
            resolve(JSON.parse(s));
        } catch (_) {
            reject(s);
        }
    } else {
        const fr = new FileReader();
        fr.onloadend = () => {
            if (fr.error) {
                reject(fr.error);
            } else {
                try {
                    resolve(JSON.parse(fr.result as string))
                } catch (_) {
                    resolve(fr.result)
                }
            }
        }
        fr.readAsText(new Blob([new Uint8Array(ab)]));
    }
});