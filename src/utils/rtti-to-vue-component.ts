import { compileTemplate } from 'pug';
import { compile } from 'vue-template-compiler';
import * as transpile from 'vue-template-es2015-compiler';

import { Action, RTTI } from '@crud/types';
import RowState from '@utils/row-state';

const componentMarkupSingle: compileTemplate = require('@templates/components/single.pug');
const componentMarkupTable: compileTemplate = require('@templates/components/table.pug');

const { EDIT, NONE, REMOVE, SAVE } = RowState;

function toFunction(code) {
    return `(function(){${code}})`;
}

export default ({ props, displayInfo }: RTTI, can: { [action in Action]: boolean }) => {
    const single = !!(displayInfo && displayInfo.single);
    const vueTemplate = (single ? componentMarkupSingle : componentMarkupTable)({
        single,
        props,
        EDIT, NONE, REMOVE, SAVE,
        can
    });

    const { render, staticRenderFns } = compile(vueTemplate, { preserveWhitespace: false });
    return transpile(`(function(){
return {staticRenderFns:[${staticRenderFns.map(toFunction)}],render:${toFunction(render)}};
})()`);
}