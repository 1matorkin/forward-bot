import md5 from '@utils/md5';

export default function (url: string) {
    return 'remote_data_' + md5(url, true);
}