import stringify from '@utils/stringify-error';

export default function (s?: string): string {
    return s ? stringify(s).trim() : '';
}