import { Context } from 'koa';
import { resolve } from 'url';

import { ADMIN } from '../server/paths.config';

export default function (ctx: Context, target?: string, secure?: boolean) {
    const url = resolve(ctx.request.origin + '/', target || ADMIN + '/');
    return secure || ctx.secure || ctx.request.header['x-forwarded-proto'] === 'https' ?
        url.replace(/^http:\/\//i, 'https://') :
        url;
}