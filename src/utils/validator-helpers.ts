import * as FV from 'fastest-validator';

import { ValidatorType } from '@crud/types';

export function typeDefault(type: ValidatorType) {
    switch (type) {
        //case 'enum':
        case 'number':
            return 0;
        case 'string':
            return '';
        case 'boolean':
            return false;
        case 'array':
            return [];
        case 'date':
            return new Date();
        case 'any':
        case 'object':
            return Object.create(null);
    }
    return null;
}

const v = new FV();

export const compile = v.compile.bind(v);