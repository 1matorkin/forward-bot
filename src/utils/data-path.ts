import * as AppDirectory from 'appdirectory';
import { sync as mkDirSync } from 'make-dir';
import { join } from 'path';

export const dataPath = new AppDirectory({ appName: APP_NAME, useRoaming: true }).userData();
mkDirSync(dataPath);

export default (...pathParts: string[]): string => join(dataPath, ...pathParts);