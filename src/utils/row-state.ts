enum RowState {
    NONE = 0,
    EDIT = 1,
    SAVE = 2,
    REMOVE = 3
}
export default RowState;