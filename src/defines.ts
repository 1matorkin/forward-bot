declare const BROWSER: boolean;
declare const DEBUG: boolean;
declare const APP_NAME: string;
declare const APP_TITLE: string;
declare const v8debug: any;
declare const EntitiesFactory: (mixins: any[], debounce: any, axios: any) => any[];
declare const MenuFactory:() => any[];