import 'normalize.css';
import 'element-ui/lib/theme-chalk/index.css';
import './modular-admin-html-gh-pages/css/app-blue.css';
import './index.scss';

import axios from 'axios';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import * as deepEqual from 'fast-deep-equal';
import { decode, encode } from 'msgpack-lite';
import Vue from 'vue';
import VueRouter from 'vue-router';

import { RTTI } from '@crud/types';
import { debounce } from '@taraflex/debounce';
import ab2obj from '@utils/arraybuffer-to-obj';
import msgpackOptions from '@utils/msgpack-options';
import RowState from '@utils/row-state';
import err2str from '@utils/stringify-error';
import { compile, typeDefault } from '@utils/validator-helpers';

// @ts-ignore
import App from './App.vue';

const paramCase = require('param-case');
const sentenceCase = require('sentence-case');
const Set = require('es6-set');

Vue.use(VueRouter);
Vue.use(ElementUI, { size: 'medium', locale });

axios.defaults.headers = { 'Content-Type': 'application/x-msgpack' }
axios.defaults.responseType = 'arraybuffer';
axios.defaults.validateStatus = status => {
    return status >= 200 && status < 500;
}

const components = EntitiesFactory([{
    async created() {
        try {
            const { props } = this.rtti as RTTI;

            this.fields = Object.freeze(Object.keys(props));

            const p = Object.create(null);
            for (let k of this.fields) {
                if (!props[k].generated) {
                    p[k] = props[k];
                }
            }
            this.validate = compile(p);

            this.noneGeneratedFields = new Set(Object.keys(p));

            this.primaryFieldIndex = this.fields.findIndex(f => props[f].primary);

            await this.loadData();
        } catch (err) {
            this.onError(err);
        }
        this.inited = true;
    },
    async activated() {
        try {
            await this.loadData();
        } catch (err) {
            this.onError(err);
        }
    },
    methods: {
        editSingleRow() {
            if ((this.rtti as RTTI).displayInfo.single && this.can.PUT) {
                this.values[0].state = RowState.EDIT;
                return true;
            }
            return false;
        },
        async loadData() {
            if (!this.loading) {

                this.loading = true;
                const { data, status } = await axios.get(this.url);
                this.loading = false;

                if (status != 200 && status != 204) {
                    throw await ab2obj(data);
                }

                if (data && data.byteLength > 0) {
                    let { values } = decode(new Uint8Array(data, 0), msgpackOptions);
                    if (Array.isArray(values)) {
                        const changed = new Map();
                        const created = [];
                        if (this.can.CHANGE) {
                            const lastIndex = this.values.length - 1;
                            for (let i = 0; i <= lastIndex; ++i) {
                                const row = this.values[i];
                                if (row.state == RowState.EDIT) {
                                    const prime = row.v[this.primaryFieldIndex];
                                    if (prime != null) {
                                        changed.set(prime, row.v);
                                    } else if (i < lastIndex || !deepEqual(row.v, this.genDefault())) {
                                        created.push({ v: row.v, i });
                                    }
                                }
                            }
                        }
                        if (changed.size > 0) {
                            values = values.map(v => {
                                const oldRow = changed.get(v[this.primaryFieldIndex]);
                                if (oldRow) {
                                    const row = { state: RowState.EDIT, errors: [], v: oldRow };
                                    try {
                                        this.row2Obj(row);
                                    } catch (e) {
                                        this.processErrors(e, row);
                                    }
                                    return row;
                                }
                                return { state: RowState.NONE, errors: [], v }
                            });
                            changed.clear();
                        } else {
                            values = values.map(v => ({ state: RowState.NONE, errors: [], v }));
                        }
                        for (let { v, i } of created) {
                            values.splice(Math.min(i, values.length), 0, { state: RowState.EDIT, errors: [], v });
                        }
                        this.values = values;
                    }
                }

                if (this.values.length < 1) {
                    this.add();
                } else {
                    this.editSingleRow();
                }
            }
        },
        edit(row) {
            row.state = this.isNew(row) || this.can.PUT ? RowState.EDIT : RowState.NONE;
        },
        async save(row) {
            if (this.can.CHANGE) {
                try {
                    row.state = RowState.SAVE;

                    const o = this.row2Obj(row);
                    this.cleanErrors(row);

                    const bo = encode(o, msgpackOptions);
                    const primary = this.getPrimary(row);
                    const { data, status } = primary == null ?
                        await axios.post(this.url, bo) :
                        await axios.put(this.url + '/' + primary, bo);

                    if (status != 201) {
                        throw await ab2obj(data);
                    }

                    const ndata = decode(new Uint8Array(data, 0), msgpackOptions);
                    for (let k in ndata) {
                        row.v[this.fields.indexOf(k)] = ndata[k];
                    }

                    if (!this.editSingleRow()) {
                        row.state = RowState.NONE;
                    }
                } catch (e) {
                    this.onError(e, row);
                    this.edit(row);
                }
            }
        },
        async remove(row, i: number) {
            const primary = this.getPrimary(row);
            if (primary == null) {
                this.removeRow(i);
            } else if (this.can.DELETE) {
                try {
                    row.state = RowState.REMOVE;
                    const { data, status } = await axios.delete(this.url + '/' + primary);
                    if (status != 204) {
                        throw await ab2obj(data);
                    }
                    this.removeRow(i);
                } catch (e) {
                    this.onError(e, row);
                    row.state = RowState.NONE;
                }
            }
        },
        async removeAll() {
            if (this.inited && this.can.DELETE) {
                try {
                    this.inited = false;
                    for (let i = this.values.length - 1; i >= 0; --i) {
                        await this.remove(this.values[i], i);
                    }
                } finally {
                    this.inited = true;
                }
            }
        },
        add() {
            if (this.can.POST) {
                const def = this.genDefault();
                if (this.values.length > 0 && deepEqual(this.values[this.values.length - 1].v, def)) {
                    const row = this.values[this.values.length - 1];
                    row.state = RowState.EDIT;
                    this.cleanErrors(row);
                } else {
                    this.values.push({ state: RowState.EDIT, errors: [], v: def });
                }
            }
        },
        removeRow(i: number) {
            this.values.splice(i, 1);
            if (this.values.length < 1) {
                this.add();
            }
        },
        genDefault() {
            const a = [];
            const { props } = this.rtti as RTTI;
            for (let field of this.fields) {
                const f = props[field];
                a.push(f.generated ? undefined : (f.default === undefined ? typeDefault(f.type) : f.default));
            }
            return a;
        },
        isNew(row) {
            return this.getPrimary(row) == null;
        },
        getPrimary(row) {
            return row.v[this.primaryFieldIndex];
        },
        cleanErrors(row) {
            row && row.errors.splice(0, row.errors.length);
        },
        onError(errors, row) {
            this.loading = false;
            errors = errors.errors || errors;
            if (Array.isArray(errors)) {
                this.cleanErrors(row);
                this.processErrors(errors, row);
                if (row && row.errors.length > 0) {
                    this.edit(row);
                }
            } else {
                this.alert(errors);
            }
        },
        row2Obj(row) {
            const o = Object.create(null);
            for (let i = 0; i < this.fields.length; ++i) {
                const { props } = this.rtti as RTTI;
                const field = this.fields[i];
                if (!props[field].generated) {
                    o[field] = row.v[i];
                }
            }
            const validateResult = this.validate(o);
            if (validateResult !== true) {
                throw validateResult;
            }
            return o;
        },
        processErrors(errors: any[], row) {
            for (let error of errors) {
                try {
                    if (!this.noneGeneratedFields.has(error.field)) {
                        throw 0;
                    }
                    row.errors[this.fields.indexOf(error.field)] = error.message;
                } catch (_) {
                    this.alert(error);
                }
            }
        },
        alert(err) {
            this.$notify.error({
                message: err2str(err),
                position: 'bottom-right',
                duration: 0
            });
        }
    }
}], debounce, axios);

const routes = Object.freeze(components.map(v => {
    const name = sentenceCase(v.name);
    return {
        path: '/' + paramCase(v.name),
        icon: v.icon,
        name,
        component: v,
    }
}));

const router = new VueRouter({
    routes: [
        ...routes,
        (routes.length > 0 ? { path: '/', redirect: routes[0].path } : { path: '/' })
    ]
});

new Vue({
    ...App,
    el: '#app',
    router,
    data: {
        routes,
        menu: MenuFactory()
    }
});